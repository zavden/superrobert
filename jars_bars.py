# --------- IMPORTS
from manimlib.imports import *
from pathlib import Path
import os
# ---------- FLAGS
RESOLUTION = ""
FLAGS = f"pm {RESOLUTION}"
SCENE = "RequestScene"


BALL_CONFIG = {
    "fill_opacity": 0,
    "stroke_width": 4,
    "stroke_color": BLUE_E,
    "radius": 0.15
}


class Jar(VGroup):
    CONFIG = {
        "stroke_width": 12,
        "balls_config": BALL_CONFIG,
    }

    def __init__(self, n_balls=15, cols=4, buff=0.2, **kwargs):
        super().__init__(**kwargs)
        jar = Polygon(*[
            [x, y, 0]
            for x, y in [
                (-1.5, 1),
                (-1.2, 1),
                (-1.2, -1),
                (1.2, -1),
                (1.2, 1),
                (1.5, 1)
            ]],
            stroke_width=self.stroke_width
        )
        jar.points = jar.points[:-1]
        # Set list
        n_elements = []
        limit = int(n_balls/cols)
        for i in range(limit):
            n_elements.append(cols)
        if n_balls % cols == 0:
            pass
        else:
            n_elements.append(n_balls % cols)
        # ---------
        balls = VGroup(*[
            VGroup(*[
                Dot(**self.balls_config)
                for _ in range(el)
            ]).arrange(RIGHT, buff=buff)
            for el in n_elements
        ]).arrange(UP, buff=buff, aligned_edge=LEFT)
        balls.next_to(jar.get_bottom(), UP)

        self.jar = jar
        self.balls = VGroup(*[
            mob for submob in balls for mob in submob
        ])
        self.balls.fade(1)
        self.add(self.jar, self.balls)
        self.n_balls = n_balls

    def get_n_balls(self):
        return self.n_balls


class BoxArray(VGroup):
    CONFIG = {
        "n_boxes": 24,
        "show_box_number": True,  # <- See the number box
        "boxes_config": {
            "width": 0.5,
            "height": 0.9,
            "color": ORANGE
        },
        "boxes_config_at": [
            (2, {"stroke_opacity": 0})  # <- Fade box number 2 (third)
        ],
        "bars_at": [6, 20],
        "bars_config": {
            "color": RED,
            "width": 0.1,
            "height": 0.7,
            "fill_opacity": 1
        },
        "ball_config": BALL_CONFIG
    }

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.boxes = self.get_boxes()
        self.bars = self.get_bars()
        self.arrays_list = [
            list(range(self.bars_at[0])),
            list(range(self.bars_at[0]+1, self.bars_at[1])),
            list(range(self.bars_at[1]+1, len(self.boxes)))
        ]
        self.balls = self.get_balls()
        for box in self.boxes_config_at:
            self.boxes[box[0]].set_style(**box[1])
        self.add(
            self.boxes,
            self.bars,
            self.balls
        )
        if self.show_box_number:
            self.boxes_numbers = self.get_number_boxes()
            self.add(self.boxes_numbers)

    def get_boxes(self):
        boxes = VGroup(*[
            Rectangle(**self.boxes_config)
            for _ in range(self.n_boxes)
        ])
        boxes.arrange(RIGHT, buff=0)
        return boxes

    def get_number_boxes(self):
        return VGroup(*[
            Text(f"{i}", font="Arial")
                .set_height(self.boxes[i].get_height() - 0.7)
                .next_to(self.boxes[i], DOWN)
            for i in range(len(self.boxes))
        ])

    def get_bars(self):
        return VGroup(*[
            Rectangle(**self.bars_config)
                .move_to(self.boxes[i])
            for i in self.bars_at if i in list(range(len(self.boxes)))
        ])

    def get_balls(self):
        return VGroup(*[
            VGroup(*[
                Dot(**self.ball_config)
                   .move_to(self.boxes[i])
                for i in limit
            ])
            for limit in self.arrays_list
        ])

    def get_array_list(self):
        return [len(i) for i in self.arrays_list]


class JarLabel(VGroup):
    def __init__(self, jar, label="A", val=0, **kwargs):
        super().__init__(**kwargs)
        letter = TextMobject(f"{label}:")
        value = Integer(val)
        VGroup(letter, value).arrange(RIGHT).next_to(jar, DOWN)
        value.add_updater(lambda mob: mob.next_to(letter, RIGHT, aligned_edge=DOWN))

        self.letter = letter
        self.value = value
        self.add(
            self.letter,
            self.value
        )

    def get_value(self):
        return self.value


class AbstractJar(Scene):
    CONFIG = {
        "box_array_config": {
            "boxes_config_at": [
                (2, {"stroke_opacity": 0})  # <- Fade box number 2 (third)
            ],
            "bars_at": [6,14]
        },
        "up_label_config": TEAL,
        "jar_config": {}
    }

    def setup(self):
        # Up label
        up_label = self.get_up_label(color=self.up_label_config)
        up_label.to_edge(UP)
        # Boxes
        boxes = BoxArray(**self.box_array_config)
        boxes.shift(UP * 1)
        # JARS
        jars = VGroup(*[Jar(i,**self.jar_config) for i in boxes.get_array_list()])
        jars.arrange(RIGHT, buff=1)
        jars.shift(DOWN * 2)
        for mob in boxes.balls:
            mob.save_state()
        # labels
        labels = VGroup(*[
            JarLabel(jar, label)
            for jar, label in zip(jars, ["A", "B", "C"])
        ])

        self.boxes = boxes
        self.jars = jars
        self.labels = labels
        self.up_label = up_label

    def construct(self):
        self.add(self.jars, self.boxes, self.labels)
        self.play(Write(self.up_label))
        self.move_balls_to_jar([1, 2], run_time=2)
        self.restore_balls([1], run_time=2)
        self.move_balls_to_jar([2, 3], run_time=2)
        self.restore_balls([1, 3], run_time=2)

        self.wait()

    def move_balls_to_jar(self, balls_jar, **kwargs):
        self.play(
            *[
                ApplyMethod(
                    self.boxes.balls[j-1][i].move_to,
                    self.jars[j-1][1][i],
                    **kwargs
                )
                for j in balls_jar for i in range(len(self.jars[j-1][1]))
            ],
            *[
                ChangeDecimalToValue(
                    self.labels[j-1].get_value(),
                    self.jars[j-1].get_n_balls(),
                    **kwargs
                )
                for j in balls_jar
            ]
        )

    def restore_balls(self, balls, **kwargs):
        self.play(
            *[
                Restore(self.boxes.balls[ball-1], **kwargs)
                for ball in balls
            ],
            *[
                ChangeDecimalToValue(
                    self.labels[j-1].get_value(),
                    0,
                    **kwargs
                )
                for j in balls
            ]
        )

    def get_up_label(self, color=RED):
        right_label = TexMobject(r"{3-1 \choose 24+3-1}")[0]
        right_label[1:4].set_color(color)
        right_label[7:10].set_color(color)
        #                        0123456789ABCDE
        left_label = TexMobject("C_{24+3-1}^{3-1}")[0]
        left_label.match_height(right_label)
        left_label[7:10].set_color(color)
        left_label[1:4].set_color(color)
        grp = VGroup(left_label, TextMobject("or"), right_label).arrange(RIGHT,buff=0.5)
        return grp


class RequestScene(AbstractJar):
    CONFIG = {
        "box_array_config": {
            "boxes_config_at": [],
            "bars_at": [0,14],
            "show_box_number": False,
            "n_boxes": 26,
        },
        "up_label_config": RED,
        "jar_config": {
            "cols": 5,
            "buff": 0.1
        }
    }

    def construct(self):
        self.add(self.jars, self.boxes, self.labels)
        self.move_balls_to_jar([1, 2], run_time=2)
        self.restore_balls([1], run_time=2)
        self.move_balls_to_jar([2, 3], run_time=2)
        self.restore_balls([1, 3], run_time=2)
        self.play(Write(self.up_label))
        self.wait()


if __name__ == '__main__':
    script_name = f"{Path(__file__).resolve()}"
    read = FLAGS
    os.system(f"manim {script_name} {SCENE} -{read}")